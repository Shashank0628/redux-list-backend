const express = require('express')
const router = express.Router()
const Course = require('../models/Course')
const validate = require('../validations')

router.get('/' , async (req , res) => {
    const courses = await Course.find({})
    res.json({courses}).status(200)
})


router.post('/' , (req , res) => {
  const {error , isValid} = validate(req.body)
  const {title , author , category , length} = req.body
  if(isValid){
    const unSavedCourse = new Course({
                                      title: title,
                                      authorId: author,
                                      length: length,
                                      category: category
                                    })
    
    unSavedCourse.save().then((course) => {
                                res.status(200).json({
                                  course: course
                                })
                              }).catch(() => {
                                res.status(500).json({
                                  error: {
                                    message: "Unable to execute database query"
                                  }
                                })
                                      })
  }
  else{
    res.status(400).json({
      error: error
    })
  }

})


router.put('/:id' , async (req , res) => {
    const id = req.params.id
    console.log(id)
    const {error , isValid}  = validate(req.body)
    if(isValid){
    try{
        const course = await Course.findByIdAndUpdate(id , req.body)
        res.status(200).json({
            course: course
        })
    }
    catch(err){
        res.status(500).json({
        error: err.message
        })
    }
    
    }else{
    res.status(400).json({
        error: error
    })
    }

})

router.delete('/:id' , async (req,res) => {
  const id = req.params.id
  
  try{
    
    await Course.findByIdAndDelete(id)
    res.status(200).json({})

  }catch(err){
    res.status(500).json({
      error: err.message
    })
  }

})

module.exports = router