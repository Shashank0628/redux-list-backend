const validate = (data) => {
    const err = {}
    if(data.title === ""){
        err.message = "title cannot be empty"
    }
    if(data.author === ""){
        err.message = "author cannot be empty"
    }
    if(data.category === ""){
        err.message = "category cannot be empty"
    }
    if(data.length === ""){
        err.message = "length cannot be empty"
    }
 
    const isValid = Object.keys(err).length === 0

    return {err , isValid}
}

module.exports = validate