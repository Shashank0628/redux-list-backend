const mongoose = require("mongoose")


const courseScheme = mongoose.Schema({
    title: {type: String , required: true},
    watchHref: {type: String , required: false},
    authorId: {type: String , required: true},
    length: {type: String , required: true},
    category: {type: String , required: true},
})

const Course = mongoose.model("Course" , courseScheme)

module.exports = Course