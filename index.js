const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
;
const routes  = require('./routes/index')
const app = express()

dotenv.config()


mongoose.connect(process.env.DB_URL , {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
   
  app.use(express.json())

  app.use((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept"
        );
        res.setHeader(
          "Access-Control-Allow-Methods",
          "GET, POST, PUT, PATCH, DELETE, OPTIONS"
        );
        next();
      })

  
  app.use('/api/courses' , routes)
  

  app.listen(process.env.PORT , () => {
        console.log("Server Established Successfully")
    })


})

